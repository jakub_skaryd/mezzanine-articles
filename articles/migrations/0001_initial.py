# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mezzanine.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_auto_20150527_1555'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='pages.Page')),
                ('content', mezzanine.core.fields.RichTextField(verbose_name='Content')),
                ('cover', mezzanine.core.fields.FileField(help_text='Image for visually signing article.', max_length=200, null=True, verbose_name='cover', blank=True)),
                ('zip_import', models.FileField(help_text="Upload a zip file containing images, and they'll be imported into this gallery.", upload_to=b'articles', null=True, verbose_name='Zip import', blank=True)),
                ('show_gallery', models.BooleanField(default=True, help_text='Controls when show or hide gallery on article page.', verbose_name='show gallery')),
                ('show_content', models.BooleanField(default=True, help_text='Controls content on page.', verbose_name='show content')),
                ('show_children_list', models.BooleanField(default=False, help_text='Controls children list on page.', verbose_name='show children')),
                ('section', models.CharField(help_text='Name used in for example class= attribute.', max_length=50, null=True, verbose_name='section', blank=True)),
                ('sections', models.ManyToManyField(related_name='_article_sections_+', null=True, verbose_name='sections', to='articles.Article', blank=True)),
            ],
            options={
                'ordering': ('_order',),
                'verbose_name': 'Article',
                'verbose_name_plural': 'Articles',
            },
            bases=('pages.page', models.Model),
        ),
        migrations.CreateModel(
            name='ArticleImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_order', mezzanine.core.fields.OrderField(null=True, verbose_name='Order')),
                ('file', mezzanine.core.fields.FileField(max_length=200, verbose_name='File')),
                ('description', models.CharField(max_length=5000, verbose_name='Description', blank=True)),
                ('article', models.ForeignKey(related_name='images', to='articles.Article')),
            ],
            options={
                'ordering': ('_order',),
                'verbose_name': 'Image',
                'verbose_name_plural': 'Images',
            },
        ),
        migrations.CreateModel(
            name='LinkedPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_order', mezzanine.core.fields.OrderField(null=True, verbose_name='Order')),
                ('article', models.ForeignKey(related_name='article_links', to='articles.Article')),
                ('page', models.ForeignKey(to='pages.Page')),
            ],
            options={
                'ordering': ('_order',),
            },
        ),
    ]
